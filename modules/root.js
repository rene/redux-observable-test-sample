import { combineEpics } from 'redux-observable';
import { combineReducers } from 'redux';
import todos, { fetchAllTodosEpic } from './todos.js';

export const rootEpic = combineEpics(
    fetchAllTodosEpic
);

export const rootReducer = combineReducers({
    todos
});