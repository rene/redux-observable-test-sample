import {
  fetchAllTodos,
  fetchAllTodosEpic,
  fetchAllTodosFulfilled,
  fetchAllTodosRejected
} from "./todos";

import { of, throwError } from "rxjs";
import testEpic from "./helper";

describe("todos", () => {
  it("should fire success action", async () => {
    const action = fetchAllTodos();
    const expectedOutputActions = [fetchAllTodosFulfilled()];

    const ajax = {
      getJSON: () => of("Some Response")
    };

    const outputActions = await testEpic(
      fetchAllTodosEpic,
      action,
      null,
      { ajax }
    );

    expect(outputActions).toEqual(expectedOutputActions);
  });

  it("should fire error action", async () => {
    const action = fetchAllTodos();

    const error = {
      xhr: {
        response: "Error response"
      }
    };

    const expectedOutputActions = [fetchAllTodosRejected(error)];

    const ajax = {
      getJSON: () => throwError(error)
    };

    const outputActions = await testEpic(
      fetchAllTodosEpic,
      action,
      null,
      { ajax }
    );

    expect(outputActions).toEqual(expectedOutputActions);
  });

});
