import { ActionsObservable } from "redux-observable";
import { Subject } from "rxjs";
import { toArray } from "rxjs/operators";

// Modified from etiennedi's work:
// https://github.com/etiennedi/redux-observable/blob/823e4526e7109f52d5f78266f4157ad4b79537db/examples/navigation/epicTestHelper.js
const testEpic = (epic, action, state = {}, dependencies = {}) => {
  const subject = new Subject();
  const action$ = new ActionsObservable(subject);
  const store = { getState: () => state };

  const promised = epic(action$, store, dependencies)
    .pipe(toArray())
    .toPromise();

  let actionList;
  if (!Array.isArray(action)) {
    actionList = [action];
  } else {
    actionList = action;
  }

  actionList.map(act => subject.next(act));
  subject.complete();

  return promised;
};

export default testEpic;
