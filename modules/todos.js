import { ofType } from "redux-observable";
import { of } from "rxjs";
import { catchError, map, mergeMap } from "rxjs/operators";

// Actions
export const FETCH_ALL_TODOS = "FETCH_ALL_TODOS";
export const FETCH_ALL_TODOS_FULFILLED = "FETCH_ALL_TODOS_FULFILLED";
export const FETCH_ALL_TODOS_REJECTED = "FETCH_ALL_TODOS_REJECTED";

export const fetchAllTodos = () => ({ type: FETCH_ALL_TODOS });
export const fetchAllTodosFulfilled = () => ({
  type: FETCH_ALL_TODOS_FULFILLED
});
export const fetchAllTodosRejected = error => ({
  type: FETCH_ALL_TODOS_REJECTED,
  payload: error.xhr.response,
  error: true
});

// Epics
export const fetchAllTodosEpic = (action$, state$, { ajax }) =>
  action$.pipe(
    ofType(FETCH_ALL_TODOS),
    mergeMap(action =>
      ajax.getJSON(`/api/todos`).pipe(
        map(response => fetchAllTodosFulfilled(response)),
        catchError(error => of(fetchAllTodosRejected(error)))
      )
    )
  );

// Reducers
const todos = (state = {}, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default todos;
